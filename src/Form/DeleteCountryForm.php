<?php

namespace Drupal\dependent_country_state\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;

/**
 * DeleteCountryForm class will delete state one by one based on state id.
 *
 * @package Drupal\dependent_country_state\Form
 */
class DeleteCountryForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_country_form';
  }

  /**
   * Store state id from the URL.
   *
   * @var sid
   */
  public $sid;

  /**
   * This function will ask confirmation.
   */
  public function getQuestion() {
    return $this->t('Do you want to delete %sid?', ['%sid' => $this->sid]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {

    return new Url('dependent_country_state.country');

  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {

    return $this->t('Only do this if you are sure!');

  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {

    return $this->t('Delete it!');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {

    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $sid = NULL) {

    $this->id = $sid;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query = \Drupal::database();

    $query->delete('dependent_country')
      ->condition('id', $this->id)
      ->execute();

    \Drupal::messenger()->addMessage($this->t('Deleted Successfully'), 'status', TRUE);
    $form_state->setRedirect('dependent_country_state.country');
  }

}
