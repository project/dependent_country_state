<?php

namespace Drupal\dependent_country_state\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dependent_country_state\services\GetData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Database\Connection;

/**
 * This is simple contact Form.
 */
class AddCForm extends FormBase {

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * Store state id from the URL incase of edit functinality.
   *
   * @var id
   */
  protected $id = 0;

  /**
   * Store state object of specific id which is given in URL.
   *
   * @var getData
   */

  protected $getData;

  /**
   * Constructor to assign object on getData variable.
   *
   * @var \Drupal\timezone\services\GetData $getData
   */

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\dependent_country_state\services\GetData $getData
   *   The getData will fetch data from data.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The request param from url to be used.
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   */
  public function __construct(GetData $getData, RequestStack $getRequest, Connection $getConnection) {
    $this->getData = $getData;
    $this->getRequest = $getRequest;
    $this->dbConnection = $getConnection;
  }

  /**
   * Define here unique form ID.
   */
  public function getFormId() {
    return "dependent_country_form_id";
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get(GetData::class),
      $container->get('request_stack'),
      $container->get('database'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->id = !empty($this->getRequest->getCurrentRequest()->query->get('id')) ? $this->getRequest->getCurrentRequest()->query->get('id') : 0;

    if ($this->id > 0) {
      $countryData = $this->getData->getAllCountry($this->id);
    }
    $form['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#placeholder' => 'India',
      '#default_value' => !empty($countryData[0]->country_name) ? $countryData[0]->country_name : $this->getRequest->getCurrentRequest()->query->get('country'),
    ];

    $form['country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country Code'),
      '#placeholder' => 'IN',
      '#default_value' => !empty($countryData[0]->country_code) ? $countryData[0]->country_code : $this->getRequest->getCurrentRequest()->query->get('country_code'),
    ];

    $form['action'] = ['#type' => 'actions'];

    $form['state_search']['submit'] = [
      '#type' => 'submit',
      '#value' => (isset($this->id) && $this->id > 0) ? $this->t('Update') : $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $country = $form_state->getValue('country');

    if (empty($country)) {
      $form_state->setErrorByName('country', $this->t('Country cannot be blank'));
    }

  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $country = $form_state->getValue('country');
    $country_code = $form_state->getValue('country_code');
    $query = $this->dbConnection;

    if (isset($this->id) && $this->id > 0) {

      $query = $query->update('dependent_country')
        ->fields(['country_name' => $country, 'country_code' => $country_code]);
      $query->condition('id', $this->id, '=');
      $success = $query->execute();

      if ($success) {
        $this->messenger()->addMessage($this->t('Country Updated Successfully'), 'status', TRUE);
      }
      else {
        $this->messenger()->addError($this->t('Country not updated, please try again.'), 'status', TRUE);
      }

      $url = Url::fromRoute('dependent_country_state.country');
      $form_state->setRedirectUrl($url);

    }
    else {

      $query = $query->insert('dependent_country')
        ->fields(['country_name', 'country_code', 'created']);
      $record = [$country, $country_code, time()];
      $query->values($record);
      $success = $query->execute();

      if ($success) {
        $this->messenger()->addMessage($this->t('Country Successfully Added'), 'status', TRUE);
      }
      else {
        $this->messenger()->addError($this->t('Country not added, please try again.'), 'status', TRUE);
      }
    }

  }

}
