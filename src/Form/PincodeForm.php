<?php

namespace Drupal\dependent_country_state\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dependent_country_state\services\GetData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * State filter form.
 */
class PincodeForm extends FormBase {

  /**
   * Store state id from the URL incase of edit functinality.
   *
   * @var id
   */
  protected $id = 0;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getData
   */
  protected $getData;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Constructor to intialized the value in variable.
   *
   * @param \Drupal\timezone\services\GetData $getData
   *   The fetch country data.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The search result set value in search field.
   */
  public function __construct(GetData $getData, RequestStack $getRequest) {
    $this->getData = $getData;
    $this->getRequest = $getRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get(GetData::class),
      $container->get('request_stack'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "pincode_search_form_id";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->id = !empty($this->getRequest->getCurrentRequest()->query->get('id')) ? $this->getRequest->getCurrentRequest()->query->get('id') : 0;

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('All Pincode List, Please search by Country, State and City are mandatory to see Pincode Result, or Area/Pincode.'),
    ];
    // Build the 'Update options' form.
    $form['state_search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter Pincode'),
      '#attributes' => ['class' => ['container-inline']],
    ];

    $countryList = $this->getData->getAllCountry();

    $options = ['' => 'Select Country'];

    foreach ($countryList as $value) {
      $options[$value->id] = $value->country_name;
    }

    $form['state_search']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $options,
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('country')) ? $this->getRequest->getCurrentRequest()->query->get('country') : '',
      '#ajax' => [
        'callback' => '::statesList',
        'effect' => 'fade',
        'event' => 'change',
        'wrapper' => 'state_list',
        'progress' => [
          'type' => 'throbber',
          'message' => 'Loading States...',
        ],
      ],

    ];

    if ($form_state->getTriggeringElement()) {

      $countryId = $form_state->getValue('country');
      $stateList = $this->getData->getAllStateByCountryId($countryId);
      $optionState = [];

      foreach ($stateList as $value) {
        $optionState[$value->id] = $value->state_name;
      }

      $form['state_search']['state'] = [
        '#type' => 'select',
        '#title' => $this->t('State'),
        '#validated' => TRUE,
        '#prefix' => '<div id="state_list">',
        '#suffix' => '</div>',
        '#options' => $optionState,
        '#empty_option' => $this->t('- Select State -'),
        '#ajax' => [
          'callback' => '::cityList',
          'effect' => 'fade',
          'event' => 'change',
          'wrapper' => 'city_list',
          'progress' => [
            'type' => 'throbber',
            'message' => 'Loading City...',
          ],
        ],
      ];
    }
    else {

      $countryId = !empty($this->getRequest->getCurrentRequest()->query->get('country')) ? $this->getRequest->getCurrentRequest()->query->get('country') : 0;

      if ($countryId > 0) {
        $stateList = $this->getData->getAllStateByCountryId($countryId);
        $optionState = [];
        foreach ($stateList as $value) {
          $optionState[$value->id] = $value->state_name;
        }
      }
      else {
        $optionState = [];
      }
      $form['state_search']['state'] = [
        '#type' => 'select',
        '#title' => $this->t('State'),
        '#validated' => TRUE,
        '#options' => $optionState,
        '#default_value' => $this->getRequest->getCurrentRequest()->query->get('state') ?? '',
        '#prefix' => '<div id="state_list">',
        '#suffix' => '</div>',
        '#empty_option' => $this->t('- Select State -'),
      ];

    }
    $stateId = $this->getRequest->getCurrentRequest()->query->get('state') ?? 0;
    if ($stateId > 0) {
      $cityList = $this->getData->getAllCityByStateId($stateId);
      $optionCity = ['' => 'Select City'];
      foreach ($cityList as $value) {
        $optionCity[$value->id] = $value->city_name;
      }
    }
    else {
      $optionCity = [];
    }

    $form['state_search']['city'] = [
      '#type' => 'select',
      '#title' => $this->t('City'),
      '#empty_option' => $this->t('- Select City -'),
      '#validated' => TRUE,
      '#options' => $optionCity,
      '#default_value' => $this->getRequest->getCurrentRequest()->query->get('city') ?? '',
      '#prefix' => '<div id="city_list">',
      '#suffix' => '</div>',
    ];

    $form['state_search']['pincode_area'] = [
      '#type' => 'textfield',
      '#size' => '30',
      '#title' => $this->t('Pincode/ Area'),
      '#placeholder' => '110091 or New Delhi',
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('pincode_area')) ? $this->getRequest->getCurrentRequest()->query->get('pincode_area') : '',
    ];

    $form['state_search']['action'] = ['#type' => 'actions'];

    $form['state_search']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    $form['state_search']['reset_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Clear Search'),
      '#url' => Url::fromRoute('dependent_country_state.pincode'),
    ];

    return $form;
  }

  /**
   * Ajax call back method for City list.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function cityList(array $form, FormStateInterface $form_state) {

    $stateId = $form_state->getValue('state');
    $cityList = $this->getData->getAllCityByStateId($stateId);

    $optionCity = ['' => 'Select City'];

    foreach ($cityList as $value) {
      $optionCity[$value->id] = $value->city_name;
    }
    $form['state_search']['city'] = [
      '#type' => 'select',
      '#name' => 'city',
      '#prefix' => '<div id="city_list">',
      '#suffix' => '</div>',
      '#validated' => TRUE,
      '#title' => $this->t('City'),
      '#options' => $optionCity,
    ];

    return $form['state_search']['city'];

  }

  /**
   * Ajax call back method for state list.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function statesList(array $form, FormStateInterface $form_state) {

    return $form['state_search']['state'];
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $country = $form_state->getValue('country');
    $state = $form_state->getValue('state');
    $city = $form_state->getValue('city');
    $pincode_area = $form_state->getValue('pincode_area');

    $url = Url::fromRoute('dependent_country_state.pincode')
      ->setRouteParameters([
        'country' => $country,
        'state' => $state,
        'city' => $city,
        'pincode_area' => $pincode_area,
      ]);

    $form_state->setRedirectUrl($url);

  }

}
