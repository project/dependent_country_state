<?php

namespace Drupal\dependent_country_state\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dependent_country_state\services\GetData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * State filter form.
 */
class CityForm extends FormBase {

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getData
   */
  protected $getData;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Constructor to intialized the value in variable.
   *
   * @param \Drupal\timezone\services\GetData $getData
   *   The fetch country data.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The search result set value in search field.
   */
  public function __construct(GetData $getData, RequestStack $getRequest) {
    $this->getData = $getData;
    $this->getRequest = $getRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get(GetData::class),
      $container->get('request_stack'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "state_form_id";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('All States List, Please search by Country & State are mandatory to see City Result.'),
    ];
    // Build the 'Update options' form.
    $form['state_search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter State'),
      '#attributes' => ['class' => ['container-inline']],
    ];

    $countryList = $this->getData->getAllCountry();

    $options = ['' => 'Select Country'];

    foreach ($countryList as $value) {
      $options[$value->id] = $value->country_name;
    }

    $form['state_search']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $options,
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('country')) ? $this->getRequest->getCurrentRequest()->query->get('country') : '',
      '#ajax' => [
        'callback' => '::statesList',
        'effect' => 'fade',
        'event' => 'change',
        'wrapper' => 'state_list',
        'progress' => [
          'type' => 'throbber',
          'message' => 'Loading States...',
        ],
      ],

    ];

    $countryId = $this->getRequest->getCurrentRequest()->query->get('country');

    if (isset($countryId) && !empty($countryId)) {

      $stateList = $this->getData->getAllStateByCountryId($countryId);

      $optionState = ['' => 'Select State'];
      foreach ($stateList as $value) {
        $optionState[$value->id] = $value->state_name;
      }
    }
    else {
      $optionState = ['' => 'Select State'];
    }

    $form['state_search']['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#validated' => TRUE,
      '#prefix' => '<div id="state_list">',
      '#suffix' => '</div>',
      '#options' => $optionState,
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('state')) ? $this->getRequest->getCurrentRequest()->query->get('state') : '',
    ];

    $form['state_search']['city'] = [
      '#type' => 'textfield',
      '#size' => '30',
      '#title' => $this->t('City'),
      '#placeholder' => 'New Delhi',
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('city')) ? $this->getRequest->getCurrentRequest()->query->get('city') : '',
    ];

    $form['state_search']['action'] = ['#type' => 'actions'];

    $form['state_search']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    $form['state_search']['reset_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Clear Search'),
      '#url' => Url::fromRoute('dependent_country_state.city'),
    ];

    return $form;
  }

  /**
   * Ajax call back method for state list.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function statesList(array $form, FormStateInterface $form_state) {

    $countryId = $form_state->getValue('country');

    $stateList = $this->getData->getAllStateByCountryId($countryId);

    $optionState = ['' => 'Select State'];
    foreach ($stateList as $value) {
      $optionState[$value->id] = $value->state_name;
    }

    $form['state_search']['state'] = [
      '#type' => 'select',
      '#name' => 'state',
      '#prefix' => '<div id="state_list">',
      '#suffix' => '</div>',
      '#validated' => TRUE,
      '#title' => $this->t('State'),
      '#options' => $optionState,
    ];

    return $form['state_search']['state'];

  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $country = $form_state->getValue('country');
    $state = $form_state->getValue('state');
    $city = $form_state->getValue('city');

    $url = Url::fromRoute('dependent_country_state.city')
      ->setRouteParameters([
        'country' => $country,
        'state' => $state,
        'city' => $city,
      ]);

    $form_state->setRedirectUrl($url);

  }

}
