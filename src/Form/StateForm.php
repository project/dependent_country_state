<?php

namespace Drupal\dependent_country_state\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dependent_country_state\services\GetData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * State filter form.
 */
class StateForm extends FormBase {

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getData
   */
  protected $getData;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Constructor to intialized the value in variable.
   *
   * @param \Drupal\timezone\services\GetData $getData
   *   The fetch country data.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The search result set value in search field.
   */
  public function __construct(GetData $getData, RequestStack $getRequest) {
    $this->getData = $getData;
    $this->getRequest = $getRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get(GetData::class),
      $container->get('request_stack'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "state_form_id";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('All States List'),
    ];
    // Build the 'Update options' form.
    $form['state_search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter State'),
      '#attributes' => ['class' => ['container-inline']],
    ];

    $countryList = $this->getData->getAllCountry();

    $options = ['' => 'Select Country'];

    foreach ($countryList as $value) {
      $options[$value->id] = $value->country_name;
    }

    $form['state_search']['state'] = [
      '#type' => 'textfield',
      '#size' => '50',
      '#title' => $this->t('State'),
      '#placeholder' => 'New Delhi',
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('state')) ? $this->getRequest->getCurrentRequest()->query->get('state') : '',
    ];

    $form['state_search']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $options,
      '#default_value' => !empty($this->getRequest->getCurrentRequest()->query->get('country')) ? $this->getRequest->getCurrentRequest()->query->get('country') : 103,
    ];
    $form['state_search']['action'] = ['#type' => 'actions'];

    $form['state_search']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    $form['state_search']['reset_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Clear Search'),
      '#url' => Url::fromRoute('dependent_country_state.state'),
    ];

    return $form;
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $state = $form_state->getValue('state');
    $country = $form_state->getValue('country');

    $url = Url::fromRoute('dependent_country_state.state')
      ->setRouteParameters(['state' => $state, 'country' => $country]);

    $form_state->setRedirectUrl($url);

  }

}
