<?php

namespace Drupal\dependent_country_state\Services;

use Drupal\Core\Database\Connection;

/**
 * Get Data Class will fetch data from database based on business logic.
 */
class GetData {

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   */
  public function __construct(Connection $getConnection) {
    $this->dbConnection = $getConnection;
  }

  /**
   * Get state details by id.
   *
   * @param int $id
   *   State id to fetch record for edit.
   */
  public function getAllStateById($id) {

    $query = $this->dbConnection->select('dependent_state"', 'st');
    $query = $query->condition('st.id', $id, '=');
    $query = $query->fields('st', ['id', 'state_name', 'countryId', 'status']);
    $result = $query->execute()->fetchAll();
    return $result;

  }

  /**
   * Get Pincode details by id.
   *
   * @param int $id
   *   Pincode id to fetch record for edit.
   */
  public function getPincodeById($id) {

    $query = $this->dbConnection->select('dependent_pincode"', 'p');
    $query = $query->condition('p.id', $id, '=');
    $query = $query->fields('p', ['id', 'area_name', 'countryId',
      'stateId', 'cityId', 'pincode',
    ]);
    $result = $query->execute()->fetch();
    return $result;

  }

  /**
   * Get state details by country id.
   *
   * @param int $id
   *   Country id to fetch record for list.
   */
  public function getAllStateByCountryId($id) {

    $query = $this->dbConnection->select('dependent_state"', 'st');
    $query = $query->condition('st.countryId', $id, '=');
    $query = $query->fields('st', ['id', 'state_name', 'countryId', 'status']);
    $result = $query->execute()->fetchAll();
    return $result;

  }

  /**
   * Get  City details by city id.
   *
   * @param int $id
   *   City id to fetch record for edit.
   */
  public function getAllCity($id = 0) {
    $query = $this->dbConnection->select('dependent_city', 'c');
    if ($id > 0) {
      $query = $query->condition('c.id', $id, '=');
    }
    $query = $query->fields('c', ['id', 'countryId',
      'stateId', 'city_name', 'status',
    ]);
    $result = $query->execute()->fetchAll();
    return $result;

  }

  /**
   * Get All City details by state id.
   *
   * @param int $id
   *   State id to fetch record for listing on ajax call.
   */
  public function getAllCityByStateId($id = 0) {
    $query = $this->dbConnection->select('dependent_city', 'c');
    if ($id > 0) {
      $query = $query->condition('c.stateId', $id, '=');
    }
    $query = $query->fields('c', ['id', 'countryId',
      'stateId', 'city_name', 'status',
    ]);
    $result = $query->execute()->fetchAll();
    return $result;

  }

  /**
   * Get Country details by id.
   *
   * @param int $id
   *   Country id to fetch record for edit.
   */
  public function getAllCountry($id = 0) {

    $query = $this->dbConnection->select('dependent_country', 'c');
    if ($id > 0) {
      $query = $query->condition('c.id', $id, '=');
    }
    $query = $query->fields('c', ['id', 'country_name', 'country_code',
      'logo', 'status', 'created',
    ]);
    $result = $query->execute()->fetchAll();
    return $result;

  }

}
