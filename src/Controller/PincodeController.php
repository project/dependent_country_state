<?php

namespace Drupal\dependent_country_state\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Render\RendererInterface;

/**
 * Pincode Controller will show all list of pincode.
 */
class PincodeController extends ControllerBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The request param from url to be used.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(Connection $getConnection, RequestStack $getRequest, RendererInterface $renderer) {
    $this->dbConnection = $getConnection;
    $this->getRequest = $getRequest;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates database class.
    return new static(
          $container->get('database'),
          $container->get('request_stack'),
          $container->get('renderer'),
    );

  }

  /**
   * List all pincode.
   */
  public function list() {

    $table['form'] = $this->formBuilder()->getForm('Drupal\dependent_country_state\Form\PincodeForm');

    $country = $this->getRequest->getCurrentRequest()->query->get('country');
    $state = $this->getRequest->getCurrentRequest()->query->get('state');
    $city = $this->getRequest->getCurrentRequest()->query->get('city');
    $pincode_area = $this->getRequest->getCurrentRequest()->query->get('pincode_area');

    $query = $this->dbConnection->select('dependent_pincode"', 'p');
    $runQuery = FALSE;

    if (!empty($country)) {
      $query = $query->condition('p.countryId', $country, '=');
      $runQuery = TRUE;
    }
    if (!empty($state)) {
      $query = $query->condition('p.stateId', $state, '=');
      $runQuery = TRUE;
    }

    if (!empty($city)) {
      $query = $query->condition('p.cityId', $city, '=');
      $runQuery = TRUE;
    }

    if (!empty($pincode_area)) {
      $orGroup = $query->orConditionGroup()
        ->condition('area_name', '%' . $pincode_area . '%', 'LIKE')
        ->condition('pincode', '%' . $pincode_area . '%', 'LIKE');
      $query->condition($orGroup);
      $runQuery = TRUE;
    }

    $query = $query->fields('p', ['id', 'area_name', 'pincode'])->range(0, 100);
    $query = $query->orderBy('id', 'DESC');

    if ($runQuery == TRUE) {
      $result = $query->execute();
    }
    else {
      $result = [];
    }

    $header = [
      '#sr_no' => 'Sr No',
      '#country_name' => 'Area Name',
      '#stats' => 'Pincode',
      '#action' => 'Action',
    ];

    $rows = [];
    $sr_no = 1;

    foreach ($result as $data) {

      $delete = Url::fromUserInput('/admin/config/country/dependent_country_state/pincode/delete/' . $data->id);
      $edit   = Url::fromUserInput('/admin/config/dependent-state-city/pincode/add?id=' . $data->id);

      $links = [
            [
              '#type' => 'dropbutton',
              '#links' => [
                [
                  'title' => $this->t('Edit'),
                  'url' => $edit,
                ],
                [
                  'title' => $this->t('Delete'),
                  'url' => $delete,
                ],
              ],
            ],
      ];

      $rows[] = [
        'sr_no' => $sr_no,
        'country_name' => $data->area_name,
        'pincode' => $data->pincode,
        'action' => $this->renderer->render($links),
      ];
      $sr_no++;
    }

    $table['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No content found.'),
    ];

    return $table;

  }

}
