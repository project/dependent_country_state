<?php

namespace Drupal\dependent_country_state\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Render\RendererInterface;

/**
 * State Controller to manage states of country.
 */
class StateController extends ControllerBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The request param from url to be used.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(Connection $getConnection, RequestStack $getRequest, RendererInterface $renderer) {
    $this->dbConnection = $getConnection;
    $this->getRequest = $getRequest;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates database class.
    return new static(
          $container->get('database'),
          $container->get('request_stack'),
          $container->get('renderer'),
    );

  }

  /**
   * List of states and can filter.
   */
  public function list() {

    $table['form'] = $this->formBuilder()->getForm('Drupal\dependent_country_state\Form\StateForm');

    $country = $this->getRequest->getCurrentRequest()->query->get('country');
    $state = $this->getRequest->getCurrentRequest()->query->get('state');

    $query = $this->dbConnection->select('dependent_state"', 'st');

    if (!empty($country)) {
      $query = $query->condition('st.countryId', $country, '=');
    }
    else {
      $query = $query->condition('st.countryId', 103, '=');
    }

    if (!empty($state)) {
      $query = $query->condition('st.state_name', '%' . $state . '%', 'LIKE');
    }

    $query = $query->fields('st', ['id', 'state_name', 'status'])->range(0, 100);
    $query = $query->orderBy('id', 'DESC');

    $result = $query->execute();

    $header = [
      '#sr_no' => 'Sr No',
      '#state' => 'State Name',
      '#stats' => 'Status',
      '#operation_1' => 'Actions',
    ];

    $rows = [];
    $sr_no = 1;

    foreach ($result as $data) {

      $delete = Url::fromUserInput('/admin/config/country/dependent_country_state/state/delete/' . $data->id);
      $edit   = Url::fromUserInput('/admin/config/dependent-state-city/state/add?id=' . $data->id);

      $links = [
            [
              '#type' => 'dropbutton',
              '#links' => [
                [
                  'title' => $this->t('Edit'),
                  'url' => $edit,
                ],
                [
                  'title' => $this->t('Delete'),
                  'url' => $delete,
                ],
              ],
            ],
      ];

      $rows[] = [
        'sr_no' => $sr_no,
        'state' => $data->state_name,
        'status' => $data->status == 1 ? ' Active ' : 'In-active',
        'operations' => $this->renderer->render($links),
      ];
      $sr_no++;

    }

    $table['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No content found.'),
    ];

    return $table;

  }

}
