<?php

namespace Drupal\dependent_country_state\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Country Controller for handing country listing etc.
 */
class APIController extends ControllerBase {

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * This variable store instace of reqequest stack to get value from url.
   *
   * @var getRequest
   */
  protected $getRequest;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   * @param Symfony\Component\HttpFoundation\RequestStack $getRequest
   *   The request param from url to be used.
   */
  public function __construct(Connection $getConnection, RequestStack $getRequest,) {
    $this->dbConnection = $getConnection;
    $this->getRequest = $getRequest;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
    );

  }

  /**
   * List all country.
   */
  public function getCountry() {

    $countryId = !empty($this->getRequest->getCurrentRequest()->query->get('id')) ? $this->getRequest->getCurrentRequest()->query->get('id') : 0;
    $countryName = !empty($this->getRequest->getCurrentRequest()->query->get('country_name')) ? $this->getRequest->getCurrentRequest()->query->get('country_name') : '';

    $query = $this->dbConnection->select('dependent_country"', 'c');
    $query = $query->fields('c', ['id', 'country_name', 'country_code', 'logo']);

    if (!empty($countryId) && $countryId > 0) {
      $query = $query->condition('c.id', $countryId, '=');
    }
    if (!empty($countryName)) {
      $query = $query->condition('c.country_name', '%' . $countryName . '%', 'like');
    }
    $query = $query->condition('c.status', 1, '=');

    $result = $query->execute()->fetchAll();
    $totalResult = count($result);
    return new JsonResponse(['total_count' => $totalResult, 'data' => $result], 200, ['Content-Type' => 'application/json']);

  }

  /**
   * List all country.
   */
  public function getState($countryId) {

    $stateName = !empty($this->getRequest->getCurrentRequest()->query->get('state_name')) ? $this->getRequest->getCurrentRequest()->query->get('state_name') : '';

    $query = $this->dbConnection->select('dependent_state"', 's');
    $query = $query->fields('s', ['id', 'state_name']);

    if (!empty($countryId) && $countryId > 0) {
      $query = $query->condition('s.countryId', $countryId, '=');
    }
    if (!empty($stateName)) {
      $query = $query->condition('s.state_name', '%' . $stateName . '%', 'like');
    }
    $query = $query->condition('s.status', 1, '=');

    $result = $query->execute()->fetchAll();
    $totalResult = count($result);
    return new JsonResponse(['total_count' => $totalResult, 'data' => $result], 200, ['Content-Type' => 'application/json']);

  }

  /**
   * List all country.
   */
  public function getCity($stateId) {

    $cityName = !empty($this->getRequest->getCurrentRequest()->query->get('city_name')) ? $this->getRequest->getCurrentRequest()->query->get('city_name') : '';

    $query = $this->dbConnection->select('dependent_city"', 'c');
    $query = $query->fields('c', ['id', 'city_name']);

    if (!empty($stateId) && $stateId > 0) {
      $query = $query->condition('c.stateId', $stateId, '=');
    }
    if (!empty($cityName)) {
      $query = $query->condition('c.city_name', '%' . $cityName . '%', 'like');
    }
    $query = $query->condition('c.status', 1, '=');

    $result = $query->execute()->fetchAll();

    $totalResult = count($result);

    return new JsonResponse(['total_count' => $totalResult, 'data' => $result], 200, ['Content-Type' => 'application/json']);

  }

  /**
   * List all country.
   */
  public function getPincode($cityId) {

    $pincode_area = !empty($this->getRequest->getCurrentRequest()->query->get('pincode_area')) ? $this->getRequest->getCurrentRequest()->query->get('pincode_area') : '';

    $query = $this->dbConnection->select('dependent_pincode"', 'p');
    $query = $query->fields('p', ['id', 'area_name', 'pincode']);

    if (!empty($cityId) && $cityId > 0) {
      $query = $query->condition('p.cityId', $cityId, '=');
    }

    if (!empty($pincode_area)) {

      $orGroup = $query->orConditionGroup()
        ->condition('p.area_name', '%' . $pincode_area . '%', 'LIKE')
        ->condition('p.pincode', '%' . $pincode_area . '%', 'LIKE');
      $query->condition($orGroup);

    }

    $result = $query->execute()->fetchAll();

    $totalResult = count($result);
    return new JsonResponse(['total_count' => $totalResult, 'data' => $result], 200, ['Content-Type' => 'application/json']);

  }

}
