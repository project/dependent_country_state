<?php

namespace Drupal\dependent_country_state\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Country Controller for handing country listing etc.
 */
class CountryController extends ControllerBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Dbconnectin variable for storing database instance.
   *
   * @var dbConnection
   */
  protected $dbConnection;

  /**
   * Construction to inilized the database object.
   *
   * @param Drupal\Core\Database\Connection $getConnection
   *   The database connection to be used.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(Connection $getConnection, RendererInterface $renderer) {
    $this->dbConnection = $getConnection;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates GetData class.
    return new static(
      $container->get('database'),
      $container->get('renderer'),
    );

  }

  /**
   * List all country.
   */
  public function list() {

    $result = $this->dbConnection->query("SELECT id, country_name,country_code,logo,status FROM dependent_country");

    $header = [
      '#sr_no' => 'Sr No',
      '#country_name' => 'Country Name',
      '#country_code' => 'Country Code',
      '#logo' => 'Logo',
      '#stats' => 'Status',
      '#action' => 'Action',
    ];

    $rows = [];
    $sr_no = 1;

    foreach ($result as $data) {

      $delete = Url::fromUserInput('/admin/config/country/dependent_country_state/country/delete/' . $data->id);
      $edit   = Url::fromUserInput('/admin/config/dependent-state-city/country/add?id=' . $data->id);

      $links = [
            [
              '#type' => 'dropbutton',
              '#links' => [
                [
                  'title' => $this->t('Edit'),
                  'url' => $edit,
                ],
                [
                  'title' => $this->t('Delete'),
                  'url' => $delete,
                ],
              ],
            ],
      ];

      $rows[] = [
        'sr_no' => $sr_no,
        'country_name' => $data->country_name,
        'country_code' => $data->country_code,
        'logo' => $data->logo,
        'status' => $data->status == 1 ? ' Active ' : 'In-active',
        'action' => $this->renderer->render($links),
      ];
      $sr_no++;
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

  }

}
